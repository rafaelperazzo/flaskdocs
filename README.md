# GERADOR DE DOCUMENTOS - flaskGenDocs
**aplicação em flask com mysql, docker e docker-compose**

**Gerador de documentos a partir de templates HTML + Fundo PNG**

**Versão: 0.9.9**

## Requisitos

* [Docker](https://docs.docker.com/get-docker)
* [Docker-compose](https://docs.docker.com/compose/install/)

## Entrada para um template
* Arquivo PNG em tamanho A4 - 1240x1754, correspondendo ao fundo do documento
* Arquivo HTML, correspondendo ao texto do documento

## Entrada para um documento

* Nome
* Textos, separados por ponto e vírgula
* Template

## Importação de documentos a partir de um CSV

* CSV com colunas separadas por ponto e vírgula;
* Primeira coluna sendo o identificador numérico do documento (CPF, SIAPE, RG, Matrícula, etc...)
* Última coluna sendo o e-mail, caso disponível. 

## Importação de documentos a partir de uma planilha do google drive

* Planilha com link compartilhado publicamente. Não pode ser um XLS ou ODS armazenado no Drive, é necessário que o documento seja uma planilha nativa do google docs. 
* Primeira coluna sendo o identificador numérico do documento (CPF, SIAPE, RG, Matrícula, etc...)
* Última coluna sendo o e-mail, caso disponível. 

## Arquivo de configuração config.ini (modelo)

```
[DEFAULT]
testing = 0
secret_key = 
working_dir = /flask/
font_path = /fonts/Times_New_Roman_Bold.ttf
theme = lux
log_file = app.log
log_write = w
log_type = 10
host= 
app_modulo= Documentos
app_sistema = flaskDocsGen
pdf_dir = docs/
telegram_token = 
iniciar_telegram = 0
nome_administrador = Administrador do Sistema
email_administrador = seuemail@email.com
username_administrador = admin
senha_administrador = autoridade

[DB]
db_user = root
db_database = flask
db_host = db_flask
db_password = 

[EMAIL]
mail_server = smtp.gmail.com
mail_port = 465
mail_username = 
mail_password = 

```

## Saída

* http://host:8000

## Baixando o projeto

* Dentro de uma pasta onde serão armazenados os arquivo do projeto digite: 

```
git clone https://gitlab.com/rafaelperazzo/flaskdocs.git .
```

## Antes de executar

* Copie o arquivo flask/config.ini.sample para flask/config.ini

```
cp flask/config.ini.sample flask/config.ini
```

* Configure o arquivo flask/config.ini de acordo com suas necessidades
* Crie a pasta mysql

```
mkdir mysql
```

## Como executar

```
docker-compose up -d
```

## Usuário e senha iniciais

* Usuário: admin
* Senha: autoridade

**Escolha uma nova senha no primeiro acesso através do menu usuário-listar-editar**

## Autor

* Prof. Rafael Perazzo Barbosa Mota ( rafael.mota (at) ufca.edu.br )

## RECURSOS
- [x] Gerenciamento de usuários (criar, alterar, listar e excluir)
- [x] Gerenciamento de templates (criar, alterar,listar, visualizar, excluir e editar HTML)
- [x] Gerenciamento de documentos (criar, alterar,listar,baixar e excluir)
- [x] Busca pública de documentos (por CPF)
- [x] Geração de documentos (PDF) a partir da busca pública
- [x] Importação de documentos a partir de um arquivo CSV
- [x] Importação de documentos a partir de um link do Google Drive
- [x] Consulta por Telegram
- [ ] Consulta por API Json
- [ ] Enviar documento por e-mail
- [ ] Envio de mensagens informativas

## TODO
- [x] Implementar permissões - Documentos: Apenas os donos gerenciam (ver e excluir)
- [x] Implementar permissões - Templates: Apenas os donos gerenciam (ver e excluir)
- [x] Templates: Apenas donos editam
- [x] Documentos: Apenas donos editam
- [x] Exclusão de template deve excluir os arquivos em disco
- [x] Criar formulário de importação de documentos
- [x] Colocar no config.ini as opções para nome do módulo
- [x] Importação de documentos: Evitar duplicados - Como saber se é uma duplicação ?
- [x] Converter HTML para PDF com o pdfkit e wkhtmltopdf (para consultas públicas)
- [x] Upload de template para a pasta templates/
- [x] Edição de template, com efeitos nos arquivos
- [x] Configuração portrait e landscape de acordo com as dimensões da imagem
- [x] Redimensionar imagem de template para (1710x1209)px
- [x] Importar do google docs
- [x] Ver arquivos de templates (PNG) na listagem
- [x] Ajustar o visual
- [x] Corrigir link do token de autenticidade gerado no PDF
- [x] Ver arquivos de templates (HTML) na listagem
- [x] Editar template diretamente no formulário de template
- [x] Envio do arquivo PDF pelo telegram
- [x] Incluir uma coluna de identificador para todos os documentos: o nome do documento
- [x] Corrigir Host do template
- [ ] Visualizar template -> Corrigir a origem do arquivo HTML
- [ ] Gerenciamento de papeis de usuários (adicionar, excluir, atribuir)
- [ ] Implementar recurso de e-mail com threads

## LINKS

* <https://www.papersizes.org/a-sizes-in-pixels.htm>
* <https://pythonhosted.org/Flask-Uploads/>
* <https://flask-sqlalchemy.palletsprojects.com/en/2.x/queries/>
* <https://github.com/kristoferjoseph/flexboxgrid>
* <http://flexboxgrid.com/>
* <https://flask.palletsprojects.com/en/2.0.x/>
* <https://www.gnu.org/licenses/licenses.html>
* <https://flask.palletsprojects.com/en/2.0.x/api/>