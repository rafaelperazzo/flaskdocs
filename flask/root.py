'''

 Gerador de documentos a partir de templates HTML + Fundo PNG
    Copyright (C) 2021  Rafael Perazzo Barbosa Mota

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

'''
from flask import Flask
from waitress import serve
from flask import render_template
from flask import request,url_for,send_from_directory,redirect,flash,Markup,Response,session
from flask_httpauth import HTTPBasicAuth
import datetime
import os
import logging
from flask_mail import Mail
from flask_mail import Message
from flask_uploads import *
from PIL import Image
from forms import criarUsuario as novoUsuario
from forms import criarTemplate as FormTemplate
from forms import criarDocumento as FormDocumento
from forms import consultarDocumento as FormConsultarDocumento
from forms import importarDocumentos as FormImportacao
from forms import importarDocumentosDrive as FormImportacaoDrive
from forms import modificarHtmlTemplate as FormModificarHTML
from flask_bootstrap import Bootstrap
import hashlib
from flask_migrate import Migrate
from flask_wtf.csrf import CSRFProtect
import configparser
from pathlib import Path
import csv
import string
import random
import shutil
import pdfkit
import pandas as pd
import requests
import re
import telebot
import threading
import semantic_version

app = Flask(__name__)
bootstrap = Bootstrap(app)
CSRFProtect(app)
config = configparser.ConfigParser()
config.read('/flask/config.ini')

VERSION = semantic_version.Version('0.9.9')
app.config['SERVER_NAME'] = config['DEFAULT']['host']

from models import database
db = database.db
migrate = Migrate()
migrate.init_app(app,db)

WORKING_DIR= config['DEFAULT']['working_dir']
FONT_PATH = config['DEFAULT']['font_path']
IMPORT_PATH = config['DEFAULT']['working_dir'] + 'imports/'
HOSTNAME = config['DEFAULT']['host']
APP_MODULO = config['DEFAULT']['app_modulo']
APP_SISTEMA = config['DEFAULT']['app_sistema']
PDF_DIR = WORKING_DIR + config['DEFAULT']['pdf_dir']

#HttpAuth
auth = HTTPBasicAuth()

#E-MAIL
mail = Mail(app)
app.config['MAIL_SERVER'] = config['EMAIL']['mail_server']
app.config['MAIL_PORT'] = config['EMAIL']['mail_port']
app.config['MAIL_USERNAME'] = config['EMAIL']['mail_username']
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_DEFAULT_SENDER'] = ''
app.config['MAIL_PASSWORD'] = config['EMAIL']['mail_password']
mail = Mail(app)

#LOG
logging.basicConfig(filename=WORKING_DIR + config['DEFAULT']['log_file'], filemode=config['DEFAULT']['log_write'], format='%(asctime)s %(name)s - %(levelname)s - %(message)s',level=int(config['DEFAULT']['log_type']))

'''
https://flask-wtf.readthedocs.io/en/stable/index.html
https://wtforms.readthedocs.io/en/2.3.x/
'''

#BANCO DE DADOS
DB_USER = config['DB']['db_user']
DB_DATABASE = config['DB']['db_database']
DB_PASSWORD = config['DB']['db_password']
DB_HOST = config['DB']['db_host']
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://' + DB_USER + ':' + DB_PASSWORD + '@' + DB_HOST + '/' + DB_DATABASE + '?charset=utf8mb4'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = os.urandom(24)
db.init_app(app)

#Carregando tabela de usuários
from models import user as Usuarios
from models import documentos as Documentos

#TEMA
app.config['BOOTSTRAP_BOOTSWATCH_THEME'] = config['DEFAULT']['theme']

#TESTING
TESTING = 0
app.config['TESTING'] = 0
try:
    if int(config['DEFAULT']['testing'])==0:
        app.config['TESTING'] = 0
    else:
        app.config['TESTING'] = 1
except:
    app.config['TESTING'] = 0

app.config['UPLOADS_DEFAULT_DEST'] = WORKING_DIR + 'upload'
app.config['UPLOADS_AUTOSERVE'] = True
app.config['PATH_PNG'] = WORKING_DIR + 'static/images'

modelos = UploadSet('png',extensions=('png'),default_dest=lambda x: app.config['PATH_PNG'])
modelos_html = UploadSet(name='html',extensions=('html'))
modelos_csv = UploadSet(name='csv',extensions=('csv'))
configure_uploads(app,(modelos,modelos_html,modelos_csv))

#TEMPLATES
app.config['TEMPLATES_AUTO_RELOAD'] = True

#DADOS INICIAIS DE USUÁRIO
try:
    app.config['NOME_ADMINISTRADOR'] = config['DEFAULT']['nome_administrador']
    app.config['EMAIL_ADMINISTRADOR'] = config['DEFAULT']['email_administrador']
    app.config['USERNAME_ADMINISTRADOR'] = config['DEFAULT']['username_administrador']
    app.config['SENHA_ADMINISTRADOR'] = config['DEFAULT']['senha_administrador']
except:
    app.config['NOME_ADMINISTRADOR'] = "Administrador do sistema"
    app.config['EMAIL_ADMINISTRADOR'] = "seuemail@email.com"
    app.config['USERNAME_ADMINISTRADOR'] = "admin"
    app.config['SENHA_ADMINISTRADOR'] = "autoridade"

options = {
    'page-size': 'A4',
    'orientation': 'portrait',
    'margin-top': '2mm',
    'margin-right': '0mm',
    'margin-bottom': '0mm',
    'margin-left': '7mm',
    'encoding': "UTF-8",
    'quiet': '',
    'custom-header' : [
        ('Accept-Encoding', 'gzip')
    ],    
    'no-outline': None
}

app.debug = False

def link2csv(url,DIR,nomearquivo):
    #Baixando arquivo
    arquivo = DIR + nomearquivo
    arquivo_temp = DIR + 'temp.ods'
    try:
        r = requests.get(url, allow_redirects=True,timeout=15)
        open(arquivo_temp, 'wb').write(r.content)
        #Importando em DF
        df = pd.read_excel(arquivo_temp, engine='odf',index_col=0)
        df.to_csv(arquivo,sep=';')
    except Exception as e:
        flash("Erro ao importar do google drive: " + str(e),'erro')
        return("")
    return arquivo

def add_role_to_user(user_id,role_id):
    user_role = Usuarios.Roles_users(user_id=user_id,role_id=role_id)
    db.session.add(user_role)
    comitar()

@app.before_first_request
def inicializar_bd():
    session['modulo'] = APP_MODULO
    session['sistema'] = APP_SISTEMA
    session['versao'] = str(VERSION)
    #db.drop_all()
    db.create_all()
    if (len(Usuarios.Users.query.all())==0):
        #Criando as permissões admin e user
        roleAdmin = Usuarios.Roles(name='admin',description='Administrador do Sistema')
        roleUser = Usuarios.Roles(name='user',description='Usuario do Sistema')
        #Criando usuário admin
        senha_admin = str(app.config['SENHA_ADMINISTRADOR'])
        admin = Usuarios.Users(name=app.config['NOME_ADMINISTRADOR'],email=app.config['EMAIL_ADMINISTRADOR'], password=hashlib.sha1(senha_admin.encode('utf8')).hexdigest(),username=app.config['USERNAME_ADMINISTRADOR'])
        #Adicionando as permissões admin e user
        db.session.add(roleAdmin)
        comitar()
        id_role_admin = roleAdmin.id
        db.session.add(roleUser)
        comitar()
        id_role_user = roleUser.id
        db.session.add(admin)
        comitar()
        id_admin = admin.id
        #Atribuindo permissões admin e user para usuário admin
        user_role = Usuarios.Roles_users(user_id=id_admin,role_id=id_role_admin)
        db.session.add(user_role)
        user_role = Usuarios.Roles_users(user_id=id_admin,role_id=id_role_user)
        db.session.add(user_role)
        comitar()

@app.before_request
def carregar_dados_sistema():
    if (session.get('modulo')==None):
        session['modulo'] = APP_MODULO
        session['sistema'] = APP_SISTEMA
        session['versao'] = str(VERSION)

'''
Iniciar:
Executar em um python iterativo do container
from root import app
from root import db
app.app_context().push()
db.create_all()
'''

######################################################################
#TELEGRAM
######################################################################
app.config['TELEGRAM'] = config['DEFAULT']['telegram_token']
app.config['INICIAR_TELEGRAM'] = config['DEFAULT']['iniciar_telegram']
bot = telebot.TeleBot(app.config['TELEGRAM'],parse_mode="HTML")

#regexp="([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})"
#regex = ^[0-9]{3,20}$|^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}$
#regex = ^[0-9]{3,20}$
@bot.message_handler(regexp="^[0-9]{3,20}$|^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}$")
def validar_cpf_telegram(message):
    try:
        with app.app_context():
            busca = message.text
            busca = preparar_string(busca)
            #dados = Documentos.Documentos.query.filter(Documentos.Documentos.textos.like(busca)).all()
            dados = Documentos.Documentos.query.filter_by(nome=busca).all()
            if len(dados)==0:
                #bot.reply_to(message,u"Não existem documentos para seu identificador.")
                bot.send_message(message.chat.id,u"Não existem documentos para seu identificador.")
                return
            total = len(dados)
            bot.send_message(message.chat.id,str(total) + " documento(s) encontrado(s)")
            bot.send_message(message.chat.id,render_template('telegram.documentos.html',dados=dados,host=app.config['SERVER_NAME']))
            
            for dado in dados:
                link = "http://" + app.config['SERVER_NAME'] +"/docs/documento/" + dado.token + "/publico"
                logging.error(link)
                #link = url_for('documento_publico',token=dado.token)
                pdf = requests.get(link,timeout=10)
                arquivo = open(PDF_DIR + 'documento.pdf','wb')
                arquivo.write(pdf.content)
                arquivo.close()
                arquivo = open(PDF_DIR + 'documento.pdf','rb')
                bot.send_document(message.chat.id,arquivo)
                arquivo.close()
                        
    except Exception as e:
        logging.error(str(e))
        
@bot.message_handler(func=lambda message: True)
def resposta_padrao(message):
    with app.app_context():
        try:
            bot.send_message(message.chat.id,render_template('telegram.bemvindo.html'))
        except Exception as e:
            logging.error(str(e))

def iniciar_bot_telegram():
    iniciar = 0
    try:
        iniciar = int(app.config['INICIAR_TELEGRAM'])
    except:
        logging.error("Erro na configuração iniciar_telegram. Verificar config.ini")
        iniciar = 0
    if (iniciar==1):
        bot.polling()

######################################################################

@app.route('/testing/<id>',methods=['GET'])
@auth.login_required(role='admin')
def testing(id):
    modo = str(id)
    if (modo=='0'):
        app.config['TESTING'] = '0'
    else:
        app.config['TESTING'] = '1'
    return(redirect(url_for('root')))

def comitar():
    testing = int(app.config['TESTING'])
    if (testing==0):
        try:
            db.session.commit()
        except Exception as e:
            flash('Erro: ' + str(e),'erro')
            flash(u'Um e-mail foi enviado ao desenvolvedor do sistema para a solução do problema.','erro')
            return (redirect(url_for('documento_adicionar')))

@auth.verify_password
def verify_password(username, password):
    senha = hashlib.sha1(password.encode('utf-8')).hexdigest()
    linha = Usuarios.Users.query.filter_by(username=username,password=senha).all()
    if (len(linha)>0):
        return (username)
    else:
        return(False)

@auth.get_user_roles
def get_user_roles(user):
    linhas = Usuarios.Users.query.filter_by(username=auth.username()).all()
    try:
        id_usuario = linhas[0].id
    except:
        logging.error("Erro ao pegar id do usuário")
        logging.error(auth.username())
        return ([])
    linhas = Usuarios.Roles_users.query.filter_by(user_id=id_usuario).all()
    roles = []
    for linha in linhas:
        role_id = linha.role_id
        role_name = Usuarios.Roles.query.filter_by(id=role_id).first().name
        roles.append(role_name)
    session['roles'] = roles
    session['username'] = auth.username()
    return (roles)

def genToken(size=24, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase):
        return ''.join(random.choice(chars) for _ in range(size))

@app.route('/')
@auth.login_required(role=['admin','user'])
def root():
    return (render_template('index.html',titulo=u"PÁGINA PRINCIPAL",testing=app.config['TESTING'],roles=session['roles']))

@app.route('/usuario/adicionar',methods=['POST','GET'])
@auth.login_required(role='admin')
def usuario_adicionar():
    if request.method == "POST":
        form = novoUsuario.NewUserForm()
        if form.validate_on_submit(): #TUDO OK COM O FORM ? ADICIONAR AO BD
            try:
                usuario = Usuarios.Users(name=request.form['name'],email=str(request.form['email']), password=hashlib.sha1(str(request.form['password']).encode('utf-8')).hexdigest(),username=str(request.form['username']))
                db.session.add(usuario)
                comitar()
            except Exception as e:
                flash("Erro: " + str(e),'erro')
                return(render_template('form.html',form=form,action=url_for('usuario_adicionar'),titulo=u"Adicionar novo Usuário",testing=app.config['TESTING']))
            #Adicionando permissão padrão user para o usuário recem criado
            try:
                user_id = usuario.id
                user_role_id = Usuarios.Roles.query.filter_by(name='user').first().id
                add_role_to_user(user_id,user_role_id)
                flash(u"Usuário adicionado com sucesso!",'sucesso')
                return(redirect(url_for('root')))
            except Exception as e:
                flash("Erro: " + str(e))
                return(render_template('form.html',form=form,action=url_for('usuario_adicionar'),titulo=u"Adicionar novo Usuário",testing=app.config['TESTING']))

        else: #Se o formulário não estiver preenchido corretamente
            return(render_template('form.html',form=form,action=url_for('usuario_adicionar'),titulo=u"Adicionar novo Usuário",testing=app.config['TESTING']))
    else:     #Se o método for o get, abrir o formulário
        form = novoUsuario.NewUserForm(inserir='1')
        return (render_template('form.html',form=form,action=url_for('usuario_adicionar'),titulo=u"Adicionar novo Usuário",testing=app.config['TESTING']))

@app.route('/usuario/mostrarTodos',methods=['GET'])
@auth.login_required(role='admin')
def usuario_mostrarTodos():
    data = Usuarios.Users.query.order_by(Usuarios.Users.username).all()
    return(render_template('tabela.html',data=data,testing=app.config['TESTING']))

@app.route('/usuario/<id>/editar',methods=['GET','POST'])
@auth.login_required(role='admin')
def usuario_editar(id):
    form = novoUsuario.NewUserForm()
    if request.method == "POST": #gravando alterações
        if form.validate_on_submit():
            usuario = Usuarios.Users.query.get(int(id))
            usuario.username = request.form['username']
            usuario.email = request.form['email']
            senha = str(request.form['password'])
            usuario.password = hashlib.sha1(senha.encode('utf8')).hexdigest()
            usuario.name = request.form['name']
            comitar()
            return(redirect(url_for('usuario_mostrarTodos')))
        else:
            return(render_template('form.html',form=form,action=url_for('usuario_editar',id=id),titulo=u"Editando Usuário",testing=app.config['TESTING']))            
    else: #abrindo página de edição
        data = Usuarios.Users.query.filter_by(id=int(id)).first()
        form.username.data = data.username
        form.email.data = data.email
        form.password.data = data.password
        form.name.data = data.name
        return(render_template('form.html',form=form,action=url_for('usuario_editar',id=id),titulo=u"Editando Usuário",testing=app.config['TESTING']))

@app.route('/usuario/<id>/excluir',methods=['GET','POST'])
@auth.login_required(role='admin')
def usuario_excluir(id):
    Usuarios.Users.query.filter(Usuarios.Users.id==int(id)).delete()
    comitar()
    return(redirect(url_for('usuario_mostrarTodos')))

def getData(cidade):
    Meses=('janeiro','fevereiro',u'março','abril','maio','junho','julho','agosto','setembro','outubro','novembro','dezembro')
    hoje = datetime.date.today()
    dia = hoje.day
    mes = hoje.month
    ano = hoje.year
    mes = Meses[mes]
    hoje = cidade + " " + str(dia) + " de " + mes + " de " + str(ano)
    return (hoje)

def redimensionar_imagem(arquivo):
    ####################################################
    # REDIMENSIONANDO IMAGEM para A4
    ####################################################
    imagem = Image.open(modelos.path(arquivo))
    largura,altura = imagem.size
    novotamanho = (0,0)
    if (largura>altura): #LANDSCAPE - PAISAGEM
        novotamanho = (1710,1209)
    else: #PORTRAIT - RETRATO
        novotamanho = (1209,1710)
    novaimagem = imagem.resize(novotamanho)
    imagem.close()
    novaimagem.save(modelos.path(arquivo))
    

@app.route('/template/adicionar',methods=['POST','GET'])
@auth.login_required(role=['admin','user'])
def template_adicionar():
    if request.method == "POST":
        form = FormTemplate.NewTemplateForm()
        if form.validate_on_submit(): #TUDO OK COM O FORM ? ADICIONAR AO BD
            token = genToken(size=5)
            nome_arquivo_html = 'template.' + token + '.html'
            nome_arquivo_png = 'template.' + token + '.png'
            try:    
                modelos_html.save(request.files['html'],name=nome_arquivo_html)
                modelos.save(request.files['png'],name=nome_arquivo_png)
                try:
                    shutil.copy2(modelos_html.path(nome_arquivo_html),WORKING_DIR + 'templates/')
                    redimensionar_imagem(modelos.path(nome_arquivo_png))
                except Exception as e:
                    flash("Erro: " + str(e),'erro')
                    return(render_template('form.html',form=form,action=url_for('template_adicionar'),titulo=u"Adicionar novo Template",testing=app.config['TESTING']))
            except Exception as e:
                logging.error("Erro ao adicionar template: " + str(e))
                flash("Erro ao adicionar template. Verifique se os arquivos enviados são do tipo HTML e PNG: " + str(e),'erro')
                return(render_template('form.html',form=form,action=url_for('template_adicionar'),titulo=u"Adicionar novo Template",testing=app.config['TESTING']))
            template = Documentos.Templates(nome=request.form['nome'],html=nome_arquivo_html, png=nome_arquivo_png, descricao=request.form['descricao'],username=auth.username())
            db.session.add(template)
            comitar()
            flash("Template adicionado com sucesso!",'sucesso')
            return(redirect(url_for('template_listar')))
        else: #Se o formulário não estiver preenchido corretamente
            return(render_template('form.html',form=form,action=url_for('template_adicionar'),titulo=u"Adicionar novo Template",testing=app.config['TESTING']))
    else:     #Se o método for o get, abrir o formulário
        form = FormTemplate.NewTemplateForm()
        return (render_template('form.html',form=form,action=url_for('template_adicionar'),titulo=u"Adicionar novo Template",testing=app.config['TESTING']))

@app.route('/template/listar',methods=['GET'])
@auth.login_required(role=['admin','user'])
def template_listar():
    if 'admin' in session['roles']:
        data = Documentos.Templates.query.order_by(Documentos.Templates.create_date).all()
    else:
        data = Documentos.Templates.query.filter_by(username=session['username']).order_by(Documentos.Templates.create_date).all()
    return(render_template('templates.html',data=data,testing=app.config['TESTING'],titulo='Lista de Templates'))

@app.route('/template/<id>/excluir',methods=['GET','POST'])
@auth.login_required(role=['admin','user'])
def template_excluir(id):
    username = Documentos.Templates.query.get(int(id)).username
    if (username==session['username']): #Usuário atual é o criador do template ?
        arquivo = Documentos.Templates.query.get(int(id))
        if arquivo is None:
            flash("Arquivo inexistente!","erro")
        else:
            try:
                os.remove(WORKING_DIR + 'templates/' + arquivo.html)
                os.remove(modelos.path(arquivo.png))
            except Exception as e:
                flash("Erro: " + str(e),'erro')
        try:
            Documentos.Templates.query.filter(Documentos.Templates.id==int(id)).delete()
            comitar()
            flash("Template removido com sucesso!",'sucesso')
        except Exception as e:
            flash("Erro: " + str(e),'erro')
    else:
        flash(u"Você não possui permissão para apagar este template!","erro")
    return(redirect(url_for('template_listar')))

def erro(mensagem,endpoint):
    logging.error(mensagem)
    flash(mensagem,'erro')
    return redirect(url_for(endpoint))

def sucesso(mensagem,endpoint):
    flash(mensagem,'sucesso')
    return(redirect(url_for(endpoint)))

@app.route('/template/<id>/editar',methods=['GET','POST'])
@auth.login_required(role=['admin','user'])
def template_editar(id):
    ###############################################
    # VERIFICA SE O USUÁRIO PODE EDITAR O TEMPLATE
    ###############################################
    username = Documentos.Templates.query.get(int(id)).username
    if (username!=session['username']): #Usuário atual é o criador do template ?
        flash(u"Você não possui permissão para editar este template!",'erro')
        return(redirect(url_for('template_listar')))
    ###############################################
    # USUÁRIO TEM PERMISSÃO. CONTINUA A EDIÇÃO
    ###############################################
    form = FormTemplate.NewTemplateForm()
    if request.method == "POST": #gravando alterações
        if request.form['nome']!='' and request.form['descricao']!='':
            template = Documentos.Templates.query.get(int(id))
            template.descricao=request.form['descricao']
            template.nome = request.form['nome']
            ###############################################
            # ATUALIZANDO ARQUIVOS, CASO NECESSÁRIO
            ###############################################
            token = genToken(size=5)
            nome_arquivo_html = 'template.' + token + '.html'
            nome_arquivo_png = 'template.' + token + '.png'
            temp_html = template.html
            temp_png = template.png
            if (request.files['html'].filename!=''):
                try:    
                    modelos_html.save(request.files['html'],name=nome_arquivo_html)
                    shutil.copy2(modelos_html.path(nome_arquivo_html),WORKING_DIR + 'templates/')
                    template.html = nome_arquivo_html
                    os.remove(WORKING_DIR + 'templates/' + temp_html) #removendo arquivo antigo
                except Exception as e:
                    return(erro("Erro ao alterar arquivo HTML: " + str(e),'template_listar'))
            if (request.files['png'].filename!=''):
                try:
                    modelos.save(request.files['png'],name=nome_arquivo_png)
                    template.png = nome_arquivo_png
                    redimensionar_imagem(modelos.path(nome_arquivo_png))
                    os.remove(modelos.path(temp_png)) #removendo arquivo antigo
                except Exception as e:
                    return(erro("Erro ao alterar arquivo PNG: " + str(e),'template_listar'))
            ###############################################
            # GRAVANDO ALTERAÇÕES NO BANCO DE DADOS E RETORNANDO A LISTA DE TEMPLATES
            ###############################################
            try:
                comitar()
            except Exception as e:
                flash("Erro ao atualizar template no banco de dados: " + str(e),'erro')
                logging.error("Erro ao atualizar template no banco de dados: " + str(e))
                return(redirect(url_for('template_listar')))

            flash("Template alterado com sucesso!",'sucesso')
            return(redirect(url_for('template_listar')))
        else:
            flash("Preencha os dados corretamente!",'erro')
            return(render_template('form.html',form=form,action=url_for('template_editar',id=id),titulo=u"Editando Template",testing=app.config['TESTING']))            
    else: #abrindo página de edição
        data = Documentos.Templates.query.filter_by(id=int(id)).first()
        form.nome.data = data.nome
        form.descricao.data = data.descricao
        form.html.flags.required = False
        form.png.flags.required = False
        return(render_template('form.html',form=form,action=url_for('template_editar',id=id),titulo=u"Editando Template",testing=app.config['TESTING']))

@app.route('/template/<id>/visualizar',methods=['GET'])
@auth.login_required(role=['admin','user'])
def template_visualizar(id):
    template = Documentos.Templates.query.get(id)
    if (template is not None):
        try:
            arquivo = Path(modelos_html.path(template.html)).read_text()
            elementos = re.findall(r"{{[a-zA-z]*\[[0-9]\]+}}",arquivo)
            conteudo = []
            for i in range(0,len(elementos)):
                conteudo.append(genToken(10))
            return(render_template(template.html,conteudo=conteudo,imagem=modelos.url(template.png)))
        except Exception as e:
            return(erro('Erro: ' + str(e),'template_listar'))
    else:
        return(erro(u'Template não encontrado!','template_listar'))

@app.route('/template/<id>/modificar',methods=['GET','POST'])
@auth.login_required(role=['admin','user'])
def template_modificar(id):
    ###############################################
    # VERIFICA SE O USUÁRIO PODE EDITAR O TEMPLATE
    ###############################################
    username = Documentos.Templates.query.get(int(id)).username
    if (username!=session['username']): #Usuário atual é o criador do template ?
        return(erro(u"Você não possui permissão para editar este template!",'template_listar'))
    form = FormModificarHTML.EditarTemplateForm()
    if request.method == "POST": #gravando alterações
        if form.validate_on_submit():
            texto_html = request.form['html']
            try:
                #Abrir arquivo de template atual
                with open(WORKING_DIR + 'templates/' + Documentos.Templates.query.get(id).html,"w") as arquivo_html:
                    arquivo_html.write(texto_html)
                return(sucesso('Template modificado com sucesso!','template_listar'))
            except Exception as e:
                return(erro('Erro: ' + str(e),'template_listar'))
        else:
            return(erro('Preencha os dados corretamente!','template_listar'))
        return(u"Não implementado!")
    else: #Mostrando form
        data = Documentos.Templates.query.get(id)
        if (data is not None):
            arquivo_html = WORKING_DIR + 'templates/' + data.html
            try:
                texto_html = Path(arquivo_html).read_text()
                form.html.data = texto_html
                return(render_template('form.html',form=form,action=url_for('template_modificar',id=id),titulo=u"Editando HTML do Template",testing=app.config['TESTING']))
            except Exception as e:
                return(erro("Erro: " + str(e),'template_listar'))
        else:
            return(erro(u"Template não existente",'template_listar'))
    

@app.route('/documento/adicionar',methods=['POST','GET'])
@auth.login_required(role=['admin','user'])
def documento_adicionar():
    if request.method == "POST":
        form = FormDocumento.NewDocumentoForm()
        choices = [(tpl.id,tpl.nome) for tpl in Documentos.Templates.query.with_entities(Documentos.Templates.id,Documentos.Templates.nome).order_by(Documentos.Templates.nome).all()]
        form.template.choices = choices
        if form.validate_on_submit(): #TUDO OK COM O FORM ? ADICIONAR AO BD            
            documento = Documentos.Documentos(nome=request.form['nome'],id_template=request.form['template'],textos=request.form['textos'],username=auth.username(),token=genToken())
            db.session.add(documento)
            comitar()
            return(redirect(url_for('documento_listar')))
        else: #Se o formulário não estiver preenchido corretamente
            return(render_template('form.html',form=form,action=url_for('documento_adicionar'),titulo=u"Adicionar novo Documento",testing=app.config['TESTING']))
    else:     #Se o método for o get, abrir o formulário
        form = FormDocumento.NewDocumentoForm()
        choices = [(tpl.id,tpl.nome) for tpl in Documentos.Templates.query.with_entities(Documentos.Templates.id,Documentos.Templates.nome).order_by(Documentos.Templates.nome).all()]
        form.template.choices = choices
        logging.error(choices)
        return (render_template('form.html',form=form,action=url_for('documento_adicionar'),titulo=u"Adicionar novo Documento",testing=app.config['TESTING']))

@app.route('/documento/listar',methods=['GET'])
@auth.login_required(role=['admin','user'])
def documento_listar():
    if 'admin' in session['roles']:
        data = Documentos.Documentos.query.order_by(Documentos.Documentos.create_date).all()
    else:
        data = Documentos.Documentos.query.filter_by(username=session['username']).order_by(Documentos.Documentos.create_date).all()
    return(render_template('documentos.html',data=data,testing=app.config['TESTING'],titulo='Lista de Documentos'))


@app.route('/documento/<id>/excluir',methods=['GET','POST'])
@auth.login_required(role=['admin','user'])
def documento_excluir(id):
    dono_documento = Documentos.Documentos.query.get(int(id))
    if (dono_documento.username==session['username']):
        try:
            Documentos.Documentos.query.filter(Documentos.Documentos.id==int(id)).delete()
            comitar()
            flash("Documento removido com sucesso!",'sucesso')
        except Exception as e:
            flash("Erro: " + str(e),'erro')
    else:
        flash(u"Você não tem permissão para remover este documento!",'erro')
    return(redirect(url_for('documento_listar')))

@app.route('/documento/<id>/editar',methods=['GET','POST'])
@auth.login_required(role=['admin','user'])
def documento_editar(id):
    ###############################################
    # VERIFICA SE O USUÁRIO PODE EDITAR O TEMPLATE
    ###############################################
    username = Documentos.Documentos.query.get(int(id)).username
    if (username!=session['username']): #Usuário atual é o criador do template ?
        flash(u"Você não possui permissão para editar este template!",'erro')
        return(redirect(url_for('documento_listar')))
    ###############################################
    # USUÁRIO TEM PERMISSÃO. CONTINUA A EDIÇÃO
    ###############################################
    form = FormDocumento.NewDocumentoForm()
    choices = [(tpl.id,tpl.nome) for tpl in Documentos.Templates.query.with_entities(Documentos.Templates.id,Documentos.Templates.nome).order_by(Documentos.Templates.nome).all()]
    form.template.choices = choices
    if request.method == "POST": #gravando alterações    
        if form.validate_on_submit():
            documento = Documentos.Documentos.query.get(int(id))
            documento.textos = request.form['textos']
            documento.nome = request.form['nome']
            documento.id_template=request.form['template']
            comitar()
            flash("Documento alterado com sucesso!",'sucesso')
            return(redirect(url_for('documento_listar')))
        else:
            flash("Preencha os dados corretamente!",'erro')
            return(render_template('form.html',form=form,action=url_for('documento_editar',id=id),titulo=u"Editando Documento",testing=app.config['TESTING']))            
    else: #abrindo página de edição
        data = Documentos.Documentos.query.filter_by(id=int(id)).first()
        form.textos.data = data.textos
        form.template.data = data.id_template
        form.nome.data = data.nome
        return(render_template('form.html',form=form,action=url_for('documento_editar',id=id),titulo=u"Editando Documento",testing=app.config['TESTING']))

@app.route('/documento/<id>/baixar',methods=['GET','POST'])
@auth.login_required(role='admin')
def documento_baixar(id):
    documento = Documentos.Documentos.query.get(int(id))
    if documento is None:
        logging.error("Tentativa de gerar um documento inexistente: " + str(id))
        return("Documento nao encontrado!")
    ####################################
    #MOSTRANDO DOCUMENTO
    ####################################
    textos = documento.textos
    textos = textos.split(sep=';')
    try:
        imagem_png = Image.open(modelos.path(documento.template.png))
        largura,altura = imagem_png.size
        if (largura>altura):
            options['orientation'] = 'landscape'
        else:
            options['orientation'] = 'portrait'
    except Exception as e:
        return(erro(u"Não foi possível abrir o arquivo de imagem PNG: " + str(e),'documento_listar'))
    try:
        pdfkit.from_string(render_template(documento.template.html,conteudo=textos,token=documento.token,host=HOSTNAME,imagem=modelos.url(documento.template.png)),PDF_DIR + 'documento.pdf',options=options)
        return(send_from_directory(PDF_DIR,'documento.pdf'))
    except Exception as e:
        logging.error("Erro ao gerar documento - render_template: " + documento.template.html)
        logging.error(str(e))
        flash('Erro: ' + str(e),'erro')
        return(redirect(url_for('documento_listar')))

@app.route('/documento/<token>/publico',methods=['GET','POST'])
def documento_publico(token):
    documento = Documentos.Documentos.query.filter_by(token=token).first()
    if (documento is None):
        return("Documento nao encontrado!")
    else:
        textos = documento.textos
        textos = textos.split(sep=';')
        try:
            imagem_png = Image.open(modelos.path(documento.template.png))
            largura,altura = imagem_png.size
            if (largura>altura):
                options['orientation'] = 'landscape'
            else:
                options['orientation'] = 'portrait'
        except Exception as e:
            return(erro(u"Não foi possível abrir o arquivo de imagem PNG: " + str(e),'documento_listar'))
        try:
            pdfkit.from_string(render_template(documento.template.html,conteudo=textos,token=documento.token,host=HOSTNAME,imagem=modelos.url(documento.template.png)),PDF_DIR + 'documento.pdf',options=options)
            return(send_from_directory(PDF_DIR,'documento.pdf'))
        except Exception as e:
            flash("Erro: " + str(e),'erro')
            logging.error("Erro: " + str(e))
            return(redirect(url_for('documento_listar')))

@app.route('/admin/importarDocumentos',methods=['GET','POST'])
@auth.login_required(role=['admin','user'])
def importar_documentos_form():
    dica = """O documento deve ter as colunas separadas por vírgula e a primeira coluna deve ser um identificador 
            como o CPG, RG, Matrícula ou SIAPE, por exemplo. """
    if request.method == "POST": #Processar importação
        form = FormImportacao.ImportarDocumentosForm()
        choices = [(tpl.id,tpl.nome) for tpl in Documentos.Templates.query.with_entities(Documentos.Templates.id,Documentos.Templates.nome).order_by(Documentos.Templates.nome).all()]
        form.template.choices = choices
        if form.validate_on_submit():
            template_id = request.form['template']
            token = genToken(size=6)
            arquivo_csv = 'input' + '_' + token + '.csv'
            try:
                modelos_csv.save(request.files['csv'],name=arquivo_csv)
            except Exception as e:
                flash("Erro: " + str(e))
                return(redirect(url_for('importar_documentos_form')))
            importar_documento_csv(modelos_csv.path(arquivo_csv),template_id)
            return(redirect(url_for('root')))
        else:
            choices = [(tpl.id,tpl.nome) for tpl in Documentos.Templates.query.with_entities(Documentos.Templates.id,Documentos.Templates.nome).order_by(Documentos.Templates.nome).all()]
            form.template.choices = choices
            flash(dica,'sucesso')
            return (render_template('form.html',form=form,action='/admin/importarDocumentos',titulo=u"Importar Documentos de um CSV",testing=app.config['TESTING'],dica=dica))
    else: #Abrir página com formulário
        form = FormImportacao.ImportarDocumentosForm()
        choices = [(tpl.id,tpl.nome) for tpl in Documentos.Templates.query.with_entities(Documentos.Templates.id,Documentos.Templates.nome).order_by(Documentos.Templates.nome).all()]
        form.template.choices = choices
        flash(dica,'sucesso')
        return (render_template('form.html',form=form,action='/admin/importarDocumentos',titulo=u"Importar Documentos de um CSV",testing=app.config['TESTING'],dica=dica))

@app.route('/admin/importarDocumentosDrive',methods=['GET','POST'])
@auth.login_required(role=['admin','user'])
def importar_documentos_drive():
    dica = """O link deve ser de um documento nativo do google drive (Não pode ser um XLS ou ODS)"""
    if request.method == "POST": #Processar importação
        form = FormImportacaoDrive.ImportarDocumentosDriveForm()
        choices = [(tpl.id,tpl.nome) for tpl in Documentos.Templates.query.with_entities(Documentos.Templates.id,Documentos.Templates.nome).order_by(Documentos.Templates.nome).all()]
        form.template.choices = choices
        if form.validate_on_submit():
            template_id = request.form['template']
            link = str(request.form['link'])
            logging.info('1- ' + link)
            #Removendo o que tiver apos a última /
            if ('edit' in link):
                posicao = link.rfind('/')
                link = link[:posicao]
                logging.info('2- ' + link)
                link = link + '/export?format=ods'
                logging.info('3- ' + link)
            else:
                link = link + '/export?format=ods'
                logging.info('4- ' + link)
            try:
                logging.info('5- ' + link)
                arquivo_csv = link2csv(link,IMPORT_PATH,'input.drive.csv')
                importar_documento_csv(arquivo_csv,template_id)
            except Exception as e:
                return(erro('Erro: ' + str(e),'root'))
            return(redirect(url_for('root')))
        else:
            choices = [(tpl.id,tpl.nome) for tpl in Documentos.Templates.query.with_entities(Documentos.Templates.id,Documentos.Templates.nome).order_by(Documentos.Templates.nome).all()]
            form.template.choices = choices
            flash(dica,'sucesso')
            return (render_template('form.html',form=form,action=url_for('importar_documentos_drive'),titulo=u"Importar Documentos do Google Drive",testing=app.config['TESTING'],dica=dica))
    else: #Abrir página com formulário
        form = FormImportacaoDrive.ImportarDocumentosDriveForm()
        choices = [(tpl.id,tpl.nome) for tpl in Documentos.Templates.query.with_entities(Documentos.Templates.id,Documentos.Templates.nome).order_by(Documentos.Templates.nome).all()]
        form.template.choices = choices
        flash(dica,'sucesso')
        return (render_template('form.html',form=form,action=url_for('importar_documentos_drive'),titulo=u"Importar Documentos do Google Drive",testing=app.config['TESTING'],dica=dica))

'''
REMOVE TODOS OS CARACTERES ESPECIAIS E ESPAÇOS DO TEXTO
'''
def preparar_string(texto):
    return (''.join(e for e in texto if e.isalnum()))

def importar_documento_csv(arquivo,template):
    path = arquivo
    if (Path(path).is_file()): #Arquivo existe ?
        csv_file = open(path)
        csv_reader = csv.reader(csv_file,delimiter=';')
        csv_file.seek(0)
        i = 0
        #############################################
        #Obter cada linha do CSV
        #############################################
        todos = ""
        contador = 0
        for linha in csv_reader:
            if i!=0:
                todos = ""
                for coluna in linha:
                    todos = todos + ";" + coluna
                todos = todos.replace(';','',1)
                #############################################
                #Inserir linha atual no BD
                #############################################
                existe = Documentos.Documentos.query.filter_by(textos=todos).all()
                if (len(existe)==0):
                    #documento = Documentos.Documentos(nome=prefixo + '_' + linha[0],id_template=template,textos=todos,token=genToken(),username=auth.username())
                    documento = Documentos.Documentos(nome=preparar_string(linha[0]),id_template=template,textos=todos,token=genToken(),username=auth.username())
                    db.session.add(documento)
                    contador = contador + 1
            i +=1
        csv_file.close()
        try:
            comitar()
        except Exception as e:
            flash("Erro ao adicionar documentos ao BD: " + str(e))
        flash(str(contador) + " documentos importados.",'sucesso')
    else:
        flash(u"Arquivo não encontrado: " + arquivo,'erro')


@app.route('/documento/consultar',methods=['GET'])
def documento_consultar():
    form = FormConsultarDocumento.ConsultarDocumentoForm()
    return (render_template('form.html',form=form,action=url_for('documento_consultar_token'),titulo=u"Procurando documentos",testing=app.config['TESTING']))

@app.route('/documento/consultar/publico',methods=['POST'])
def documento_consultar_token():
    if request.method == "POST":
        form = FormConsultarDocumento.ConsultarDocumentoForm()
        if form.validate_on_submit(): #TUDO OK COM O FORM ? CONSULTAR BD
            busca = str(request.form['cpf'])
            busca = preparar_string(busca)
            #data = Documentos.Documentos.query.filter(Documentos.Documentos.textos.like(busca)).all()
            data = Documentos.Documentos.query.filter_by(nome=busca).all()
            return(render_template('documentos.html',data=data,testing=app.config['TESTING'],titulo='Lista de Documentos encontrados'))
        else:
            return (render_template('form.html',form=form,action=url_for('documento_consultar_token'),titulo=u"Procurando documentos",testing=app.config['TESTING']))

@app.route('/template/<id>/imagem',methods=['GET'])
@auth.login_required(role=['admin','user'])
def template_mostrar_imagem(id):
    template_png = Documentos.Templates.query.get(int(id)).png
    return(send_from_directory(app.config['PATH_PNG'],template_png))

if __name__ == "__main__":
    t1 = threading.Thread(target=iniciar_bot_telegram,name="Iniciar BOT")
    t1.start()
    serve(app, host='0.0.0.0', port=80, url_prefix='/docs')
