from flask_wtf import FlaskForm
from wtforms import *
from wtforms.fields.html5 import *
from wtforms.validators import DataRequired,Email,Length,ValidationError,InputRequired,EqualTo,Optional

class ImportarDocumentosForm(FlaskForm):
    csv = FileField(u'Arquivo CSV',description=u'Arquivo CSV com ; (ponto e vírgula) como separador',validators=[DataRequired()])
    template = SelectField(u'Template: ',coerce=int)
    submit = SubmitField('Importar')