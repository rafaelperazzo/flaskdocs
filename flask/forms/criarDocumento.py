from flask_wtf import FlaskForm
from wtforms import *
from wtforms.fields.html5 import *
from wtforms.validators import DataRequired,Email,Length,ValidationError,InputRequired,EqualTo
import re

def nome_check(form,field):
    identificador = str(field.data)
    resultado = re.findall("^[0-9]{3,20}$",identificador)
    if (len(resultado)==0):
        raise ValidationError(u"O identificador deve ser formado apenas por números. Um CPF, SIAPE, Matrícula, etc...")

class NewDocumentoForm(FlaskForm):
    textos = TextAreaField('Vetor Textos: ', validators=[DataRequired()])
    template = SelectField(u'Template: ',coerce=int)
    nome = StringField('Identificador do documento: ', validators=[DataRequired(),nome_check],description=u"Deve ser formado apenas por números. Sugestão: CPF, SIAPE, RG, etc...")
    submit = SubmitField('Enviar')