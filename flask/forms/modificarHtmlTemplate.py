from flask_wtf import FlaskForm
from wtforms import *
from wtforms.fields.html5 import *
from wtforms.validators import DataRequired,Email,Length,ValidationError,InputRequired,EqualTo,Optional

class EditarTemplateForm(FlaskForm):
    html = TextAreaField('HTML',description='Código HTML',validators=[DataRequired()],render_kw={'rows':'20'})
    submit = SubmitField('Enviar')