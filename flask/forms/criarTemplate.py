from flask_wtf import FlaskForm
from wtforms import *
from wtforms.fields.html5 import *
from wtforms.validators import DataRequired,Email,Length,ValidationError,InputRequired,EqualTo,Optional

class NewTemplateForm(FlaskForm):
    nome = StringField('Nome do template: ', validators=[DataRequired()])
    descricao = TextAreaField(u'Descrição do template: ', validators=[DataRequired()])
    html = FileField(u'Arquivo de template (html)',description='Arquivo HTML',validators=[InputRequired()])
    png = FileField(u'Arquivo de template (png)',description='Arquivo PNG',validators=[InputRequired()])
    submit = SubmitField('Enviar')