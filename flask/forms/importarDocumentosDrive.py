from flask_wtf import FlaskForm
from wtforms import *
from wtforms.fields.html5 import *
from wtforms.validators import DataRequired,Email,Length,ValidationError,InputRequired,EqualTo,Optional

class ImportarDocumentosDriveForm(FlaskForm):
    link = URLField(u'Link do google drive',description=u'Link do google drive - Deve estar compartilhado para todos',validators=[DataRequired()])
    template = SelectField(u'Template: ',coerce=int)
    submit = SubmitField('Importar',description='Aperte apenas uma vez e aguarde...')