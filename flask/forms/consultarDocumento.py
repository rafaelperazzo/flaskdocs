from flask_wtf import FlaskForm
from wtforms import *
from wtforms.validators import DataRequired,Length

class ConsultarDocumentoForm(FlaskForm):
    cpf = StringField('Identificador: ', description=u'CPF, RG, SIAPE, Matrícula, etc..',validators=[DataRequired()])
    submit = SubmitField('Enviar')