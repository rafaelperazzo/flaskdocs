FROM python:latest
RUN apt-get update
RUN apt-get install -y g++ build-essential libmariadbclient-dev libmariadb-dev-compat libmariadb-dev
RUN pip install --upgrade pip
RUN pip install numpy scipy pandas sympy matplotlib werkzeug Flask lxml mysqlclient unidecode requests Flask-HTTPAuth Flask-Mail pdfkit waitress bs4 PyMySQL sqlalchemy
RUN pip install semantic-version Pillow
RUN pip install Flask-WTF
RUN pip install wtforms[email]
RUN pip install Flask-SQLAlchemy
RUN pip install bootstrap-flask
RUN pip install flask-security
RUN pip install Flask-Migrate
RUN pip install Flask-Reuploaded
RUN pip install Flask-INIConfig
RUN pip install odfpy
RUN apt-get update
RUN apt-get -y install wkhtmltopdf
RUN pip install pyTelegramBotAPI
RUN pip install semantic_version
EXPOSE 80
CMD python /flask/root.py
